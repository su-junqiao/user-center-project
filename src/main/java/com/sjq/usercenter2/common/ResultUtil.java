package com.sjq.usercenter2.common;

import com.sjq.usercenter2.common.BaseResponse;

/**
 * 返回结果工具类
 *
 * @author sujunqiao
 */
public class ResultUtil {
    /**
     * 成功
     *
     * @param data 实际传输的数据
     * @param <T>  泛型
     * @return 统一返回类
     */
    public static <T> BaseResponse<T> success(T data) {
        return new BaseResponse<>(0, data, "ok");
    }

    /**
     * 成功
     *
     * @param data 实际传输的数据
     * @param <T>  泛型
     * @return 统一返回类
     */
    public static <T> BaseResponse<T> success(T data, String description) {
        return new BaseResponse<>(0, data, "ok", description);
    }


    /**
     * 失败
     *
     * @param errorCode 错误信息枚举类的对象
     * @return 统一返回类
     */
    public static BaseResponse error(ErrorCode errorCode) {
        return new BaseResponse(errorCode);
    }

    /**
     * 失败
     *
     * @param errorCode 错误信息枚举类的对象
     * @return 统一返回类
     */
    public static BaseResponse error(ErrorCode errorCode, String message, String description) {
        return new BaseResponse(errorCode.getCode(), null, message, description);
    }

    /**
     * 失败
     *
     * @param errorCode 错误信息枚举类的对象
     * @return 统一返回类
     */
    public static BaseResponse error(ErrorCode errorCode, String description) {
        return new BaseResponse(errorCode.getCode(), null, errorCode.getMessage(), description);
    }

    /**
     * 失败
     *
     * @param code        code
     * @param message     message
     * @param description description
     * @return 统一返回类
     */
    public static BaseResponse error(int code, String message, String description) {
        return new BaseResponse(code, null, message, description);
    }
}
