package com.sjq.usercenter2.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sjq.usercenter2.model.domain.User;

/**
* @author 86182
* @description 针对表【user】的数据库操作Mapper
* @createDate 2024-03-30 01:11:42
* @Entity com.sjq.usercenter2.model.domain.User
*/
public interface UserMapper extends BaseMapper<User> {

}




