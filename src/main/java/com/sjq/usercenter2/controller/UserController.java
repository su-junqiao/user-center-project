package com.sjq.usercenter2.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sjq.usercenter2.common.BaseResponse;
import com.sjq.usercenter2.common.ErrorCode;
import com.sjq.usercenter2.common.ResultUtil;
import com.sjq.usercenter2.exception.BusinessException;
import com.sjq.usercenter2.model.domain.User;
import com.sjq.usercenter2.model.domain.request.UserLoginRequest;
import com.sjq.usercenter2.model.domain.request.UserRegisterRequest;
import com.sjq.usercenter2.service.UserService;
import org.apache.commons.lang3.StringUtils;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

import static com.sjq.usercenter2.constant.UserConstant.USER_LOGIN_STATE;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = {"http://localhost:5173", "http://localhost:5174", "http://localhost:8000"}, allowCredentials = "true")
@EnableScheduling
public class UserController {
    @Resource
    private UserService userService;

    /**
     * 注册
     *
     * @param userRegisterRequest
     * @return
     */
    @PostMapping("/register")
    public BaseResponse<Long> userRegister(@RequestBody UserRegisterRequest userRegisterRequest) {
        if (userRegisterRequest == null) {
            // return ResultUtil.error(ErrorCode.PARAM_ERROR);
            throw new BusinessException(ErrorCode.NULL_ERROR, "请求参数为空");
        }
        // 校验
        String userAccount = userRegisterRequest.getUserAccount();
        String userPassword = userRegisterRequest.getUserPassword();
        String checkPassword = userRegisterRequest.getCheckPassword();
        String planetCode = userRegisterRequest.getPlanetCode();
        if (StringUtils.isAnyBlank(userAccount, userPassword, checkPassword)) {
            throw new BusinessException(ErrorCode.PARAM_ERROR, "userAccount、userPassword和checkPassword至少一个参数为空");
        }
        Long result = userService.userRegister(userAccount, userPassword, checkPassword, planetCode);
        return ResultUtil.success(result);
    }

    /**
     * 登录
     *
     * @param userLoginRequest 封装的userLoginRequest的request类型的对象
     * @param request          request对象
     * @return
     */
    @PostMapping("/login")
    public BaseResponse<User> userLogin(@RequestBody UserLoginRequest userLoginRequest, HttpServletRequest request) {
        if (userLoginRequest == null) {
            throw new BusinessException(ErrorCode.NULL_ERROR, "请求参数为空");
        }
        // 校验
        String userAccount = userLoginRequest.getUserAccount();
        String userPassword = userLoginRequest.getUserPassword();
        if (StringUtils.isAnyBlank(userAccount, userPassword)) {
            throw new BusinessException(ErrorCode.PARAM_ERROR, "userAccount、userPassword至少一个参数为空");
        }
        User result = userService.userLogin(userAccount, userPassword, request);
        return ResultUtil.success(result, "登录成功，用户为" + userAccount);
    }

    /**
     * 注销
     *
     * @param request request对象
     * @return 1
     */
    @PostMapping("/logout")
    public BaseResponse<Integer> userLogout(HttpServletRequest request) {
        if (request == null) {
            throw new BusinessException(ErrorCode.NULL_ERROR, "请求参数为空");
        }
        Integer result = userService.userLogout(request);
        return ResultUtil.success(result);
    }

    /**
     * 搜索用户，仅管理员身份的账户可用此功能
     *
     * @param username 要搜索的用户的用户名
     * @return 用户列表
     */
    @GetMapping("/search")
    public BaseResponse<List<User>> searchUsers(String username, HttpServletRequest request) {
        if (!userService.isAdmin(request)) {
            // return new ArrayList<>();
            throw new BusinessException(ErrorCode.NO_AUTHORITY, "无管理员权限");
        }
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(username)) {
            queryWrapper.like("username", username);
        }
        List<User> userList = userService.list(queryWrapper);
        List<User> result = userList.stream().map(user -> userService.getSafetyUser(user)).collect(Collectors.toList());
        return ResultUtil.success(result);
    }

    /**
     * 删除指定 id 的用户
     * @param id 用户 id
     * @param request request对象
     * @return
     */
    @PostMapping("/delete")
    public BaseResponse<Boolean> deleteUser(@RequestBody long id, HttpServletRequest request) {
        // 必须鉴权，只有管理员才可删除用户
        if (userService.isAdmin(request)) {
            throw new BusinessException(ErrorCode.NO_AUTHORITY, "无管理员权限");
        }
        if (id <= 0) {
            throw new BusinessException(ErrorCode.PARAM_ERROR, "请求参数错误");
        }
        Boolean result = userService.removeById(id);
        return ResultUtil.success(result);
    }

    /**
     * 修改用户信息
     * @param user 包含了新的用户信息的
     * @param request request对象
     * @return
     */
    @PostMapping("/update")
    public BaseResponse<Integer> updateUser(@RequestBody User user, HttpServletRequest request) {
        // 校验参数
        if (null == user || null == request) {
            throw new BusinessException(ErrorCode.PARAM_ERROR, "参数异常");
        }
        User loggedUser = userService.getLoggedUser(request);
        if (null == loggedUser) {
            throw new BusinessException(ErrorCode.NO_LOGIN, "用户未登录");
        }
        int result = userService.updateUser(user, loggedUser);
        return ResultUtil.success(result);
    }

    /**
     * 获取当前的登录态信息
     *
     * @param httpSession httpSession对象
     * @return 当前的登录态信息
     */
    @GetMapping("/current")
    public BaseResponse<User> getCurrentUser(HttpSession httpSession) {
        if (httpSession == null) {
            System.out.println("null");
        }
        Object userObj = httpSession.getAttribute(USER_LOGIN_STATE);
        User currentUser = (User) userObj;
        if (currentUser == null) {
            throw new BusinessException(ErrorCode.PARAM_ERROR, "未登录");
        }
        // 要查数据库
        long userId = currentUser.getId();
        // TODO: 2024/04/05 校验用户的状态，是否被封号
        User user = userService.getById(userId);
        return ResultUtil.success(userService.getSafetyUser(user), "success");
    }

    /**
     * 按标签列表来查询具有这些标签的用户
     * @param tagNameList 标签列表
     * @return 具有这些标签的用户
     */
    @GetMapping("/search/tags")
    public BaseResponse<List<User>> searchUsersByTags(@RequestParam List<String> tagNameList) {
        if (CollectionUtils.isEmpty(tagNameList)) {
            throw new BusinessException(ErrorCode.PARAM_ERROR, "标签名列表为空");
        }
        List<User> userList = userService.searchUsersByTags(tagNameList);
        return ResultUtil.success(userList, "success");
    }

    @GetMapping("/recommend")
    public BaseResponse<Page<User>> recommendUsers(long pageSize, long pageIndex, HttpServletRequest request) {
        // 需要分页查询
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        Page<User> userList = userService.page(new Page<>(pageIndex, pageSize), queryWrapper);
        return ResultUtil.success(userList);
    }

}
