package com.sjq.usercenter2.model.domain.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 注册请求封装
 */
@Data
public class UserRegisterRequest implements Serializable {
    private static final String serialVersionUID = "1252352518L";
    private String userAccount;
    private String userPassword;
    private String checkPassword;
    private String planetCode;
}
