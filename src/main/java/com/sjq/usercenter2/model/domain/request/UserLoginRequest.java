package com.sjq.usercenter2.model.domain.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserLoginRequest implements Serializable {
    private static final String serialVersionUID = "125232518L";
    private String userAccount;
    private String userPassword;
}
