package com.sjq.usercenter2;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@SpringBootApplication
@MapperScan("com.sjq.usercenter2.mapper")
@EnableRedisHttpSession
public class Usercenter2Application {

    public static void main(String[] args) {
        SpringApplication.run(Usercenter2Application.class, args);
    }

}
