package com.sjq.usercenter2.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sjq.usercenter2.common.ErrorCode;
import com.sjq.usercenter2.common.ResultUtil;
import com.sjq.usercenter2.exception.BusinessException;
import com.sjq.usercenter2.model.domain.User;
import com.sjq.usercenter2.service.UserService;
import com.sjq.usercenter2.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.sjq.usercenter2.constant.UserConstant.ADMIN_ROLE;
import static com.sjq.usercenter2.constant.UserConstant.USER_LOGIN_STATE;

/**
 * @author 86182
 * @description 针对表【user】的数据库操作Service实现
 * @createDate 2024-03-30 01:11:42
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
        implements UserService {

    @Resource
    private UserMapper userMapper;
    /**
     * 盐值，用于混乱密码
     */
    private static final String SALT = "sujunqiao";

    @Override
    public long userRegister(String userAccount, String userPassword, String checkPassword, String planetCode) {
        // 1.1 各种校验
        if (StringUtils.isAnyBlank(userAccount, userPassword, checkPassword)) {
            throw new BusinessException(ErrorCode.NULL_ERROR, "userAccount、userPassword和checkPassword至少一个参数为空");
        }
        if (userAccount.length() < 4) { // 账户不得小于4个字符
            throw new BusinessException(ErrorCode.PARAM_ERROR, "userAccount长度不得小于4个字符");
        }
        if (userPassword.length() < 8 || checkPassword.length() < 8) { // 密码不得小于8个字符
            throw new BusinessException(ErrorCode.PARAM_ERROR, "userPassword不得小于8个字符");
        }
        // 1.2 账户不能包含特殊字符
        String validPattern = "[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        Matcher matcher = Pattern.compile(validPattern).matcher(userAccount);
        if (matcher.find()) { // 包含特殊字符
            throw new BusinessException(ErrorCode.PARAM_ERROR, "userAccount长度不得小于4个字符");
        }
        // 1.3 密码和校验密码要相同
        if (!userPassword.equals(checkPassword)) {
            throw new BusinessException(ErrorCode.PARAM_ERROR, "两次输入的密码不一致");
        }
        // 1.4 账户不能重复
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userAccount", userAccount);
        if (userMapper.selectCount(queryWrapper) > 0) { // 代表查询到了记录，即存在相同的账户
            throw new BusinessException(ErrorCode.PARAM_ERROR, "账户重复");
        }
        // 1.5 星球编号长度不得大于5、必须唯一、不得重复
        if (planetCode.length() > 5) {
            throw new BusinessException(ErrorCode.PARAM_ERROR, "星球编号长度不得大于5");
        }
        queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("planetCode", planetCode);
        if (userMapper.selectCount(queryWrapper) > 0) { // 代表查询到了记录，即存在相同的星球编号
            throw new BusinessException(ErrorCode.PARAM_ERROR, "星球编号重复");
        }
        // 2 对用户密码进行加密
        String encryptedPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes()); // 加密后的密码
        // 3 插入数据到数据库中
        User user = new User();
        user.setUserAccount(userAccount);
        user.setUserPassword(encryptedPassword);
        user.setAvatarUrl("https://avatars.st.dl.eccdnx.com/fef49e7fa7e1997310d705b2a6158ff8dc1cdfebfull.jpg");
        user.setPlanetCode(planetCode);
        boolean saveResult = this.save(user);
        if (!saveResult) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "内部错误");
        }
        return user.getId();
    }

    @Override
    public User userLogin(String userAccount, String userPassword, HttpServletRequest request) {
        // 1.1 各种校验
        if (StringUtils.isAnyBlank(userAccount, userPassword)) {
            throw new BusinessException(ErrorCode.NULL_ERROR, "userAccount、userPassword至少一个参数为空");
        }
        if (userAccount.length() < 4) { // 账户不得小于4个字符
            throw new BusinessException(ErrorCode.PARAM_ERROR, "userAccount长度不得小于4个字符");
        }
        // 1.2 账户不能包含特殊字符
        String validPattern = "[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        Matcher matcher = Pattern.compile(validPattern).matcher(userAccount);
        if (matcher.find()) { // 包含特殊字符
            throw new BusinessException(ErrorCode.PARAM_ERROR, "userAccount包含特殊字符");
        }
        // 2 对用户密码进行加密
        String encryptedPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes()); // 加密后的密码
        // 3 查询数据库判断该用户是否存在
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userAccount", userAccount);
        queryWrapper.eq("userPassword", encryptedPassword);
        User user = userMapper.selectOne(queryWrapper);
        if (user == null) {
            // 写日志
            log.info("User login failed, userAccount cannot match userPassword.");
            // todo 这里是不能直接抛出异常让异常处理器来处理的吗？
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "系统内部异常");
        }
        // 登录成功
        // 4 脱敏
        User safeUser = getSafetyUser(user);
        // 5 记录用户的登录态，存储到session中
        log.info("User login success.");
        request.getSession().setAttribute(USER_LOGIN_STATE, safeUser);
        return safeUser;
    }


    @Override
    public int userLogout(HttpServletRequest request) {
        request.getSession().removeAttribute(USER_LOGIN_STATE);
        return 1;
    }

    @Override
    public int updateUser(User user, User loggedUser) {
        // todo 补充校验，如果用户没有传任何要更新的值，就直接报错，不用执行 update 语句
        // 如果是管理员的话，TA可以更新任意用户的信息，如果不是管理员的话，只能更新自己的信息
        long userId = user.getId();
        if (userId <= 0) {
            throw new BusinessException(ErrorCode.PARAM_ERROR, "请求参数错误");
        }
        // 如果不是管理员且要修改的 id 和已登录的 id 不匹配的话，抛出异常，结束
        if (!isAdmin(loggedUser) && userId != loggedUser.getId()) {
            throw new BusinessException(ErrorCode.NO_AUTHORITY, "无权限");
        }
        User oldUser = userMapper.selectById(userId);
        if (null == oldUser) {
            throw new BusinessException(ErrorCode.NULL_ERROR, "要修改的用户不存在");
        }
        // 执行修改的操作
        return userMapper.updateById(user);
    }

    @Override
    public User getSafetyUser(User originUser) {
        if (originUser == null) {
            return null;
        }
        User safeUser = new User();
        safeUser.setId(originUser.getId());
        safeUser.setUsername(originUser.getUsername());
        safeUser.setUserAccount(originUser.getUserAccount());
        safeUser.setAvatarUrl(originUser.getAvatarUrl());
        safeUser.setGender(originUser.getGender());
        // safeUser.setUserPassword("");
        safeUser.setPhone(originUser.getPhone());
        safeUser.setEmail(originUser.getEmail());
        safeUser.setUserRole(originUser.getUserRole());
        safeUser.setUserStatus(originUser.getUserStatus());
        safeUser.setCreateTime(originUser.getCreateTime());
        safeUser.setPlanetCode(originUser.getPlanetCode()); // 星球编号
        safeUser.setTags(originUser.getTags()); // 标签
        safeUser.setProfile(originUser.getProfile()); // 个人简介
        return safeUser;
    }

    /**
     * 根据标签列表搜索用户（内存实现版本）
     *
     * @param tagNameList 标签名列表
     * @return 符合条件的用户列表
     */
    @Override
    public List<User> searchUsersByTags(List<String> tagNameList) {
        // 1.校验
        if (CollectionUtils.isEmpty(tagNameList)) {
            throw new BusinessException(ErrorCode.PARAM_ERROR, "用户标签列表为空");
        }
        // 2. 查询所有用户
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        List<User> userList = userMapper.selectList(queryWrapper);
        Gson gson = new Gson();
        // 3. 在内存中判断是否包含符合要求的标签
        return userList.stream().filter(user -> {
            String tagStr = user.getTags();
            if (StringUtils.isBlank(tagStr)) {
                return false;
            }
            // tempTagNameSet 是每一个用户的标签列表
            Set<String> tempTagNameSet = gson.fromJson(tagStr, new TypeToken<Set<String>>() {
            }.getType());
            tempTagNameSet = Optional.ofNullable(tempTagNameSet).orElse(new HashSet<>()); // 校验
            for (String tagName : tagNameList) { // 遍历传来的标签列表
                // 若用户的标签列表不包含传来的标签列表中的任意一个，表示这个用户不是我们要找的用户，剔除掉
                if (!tempTagNameSet.contains(tagName)) {
                    return false;
                }
            }
            // 用户的标签列表包含所有传来的标签
            return true;
            // 脱敏
        }).map(this::getSafetyUser).collect(Collectors.toList());
    }

    @Override
    public User getLoggedUser(HttpServletRequest request) {
        if (null == request) {
            return null;
        }
        User loggedUser = (User) request.getSession().getAttribute(USER_LOGIN_STATE);
        if (null == loggedUser) {
            throw new BusinessException(ErrorCode.NO_LOGIN, "用户未登录");
        }
        return loggedUser;
    }

    @Override
    public boolean isAdmin(HttpServletRequest request){
        Object userObj = request.getSession().getAttribute(USER_LOGIN_STATE);
        User user = (User) userObj;
        return user != null && user.getUserRole() == ADMIN_ROLE;
    }

    @Override
    public boolean isAdmin(User loggedUser) {
        return loggedUser != null && loggedUser.getUserRole() == ADMIN_ROLE;
    }

    /**
     * 根据标签搜索用户（SQL实现版本）
     *
     * @param tagNameList 标签列表
     * @return 符合条件的用户列表
     */
    @Deprecated
    public List<User> searchUsersByTagsBySQL(List<String> tagNameList) {
        // 1.校验
        if (CollectionUtils.isEmpty(tagNameList)) {
            throw new BusinessException(ErrorCode.PARAM_ERROR, "用户标签列表为空");
        }
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        for (String tagName : tagNameList) {
            queryWrapper = queryWrapper.like("tags", tagName);
        }
        // 脱敏
        List<User> userList = userMapper.selectList(queryWrapper);
        return userList.stream().map(this::getSafetyUser).collect(Collectors.toList());
    }
}




