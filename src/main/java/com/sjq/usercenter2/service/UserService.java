package com.sjq.usercenter2.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sjq.usercenter2.model.domain.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.sjq.usercenter2.constant.UserConstant.ADMIN_ROLE;
import static com.sjq.usercenter2.constant.UserConstant.USER_LOGIN_STATE;

/**
 * @author 86182
 * @description 针对表【user】的数据库操作Service
 * @createDate 2024-03-30 01:11:42
 */
public interface UserService extends IService<User> {
    /**
     * 用户注册
     *
     * @param userAccount   用户帐号
     * @param userPassword  用户密码
     * @param checkPassword 校验密码
     * @param planetCode    星球编号
     * @return 新用户的 id
     */
    long userRegister(String userAccount, String userPassword, String checkPassword, String planetCode);

    /**
     * 用户登录
     *
     * @param userAccount  用户帐号
     * @param userPassword 用户密码
     * @param request      HttpServletRequest对象，用于获取session会话
     * @return 脱敏后的用户信息
     */
    User userLogin(String userAccount, String userPassword, HttpServletRequest request);

    /**
     * 用户注销
     *
     * @param request request对象
     * @return 状态
     */
    int userLogout(HttpServletRequest request);

    /**
     * 更新用户信息
     *
     * @param user 封装了更新后的信息
     * @return > 1 表示更新成功
     */
    int updateUser(User user, User loggedUser);

    /**
     * 返回将originUser脱敏后的user对象
     *
     * @param originUser 原user对象
     * @return 脱敏后的user对象
     */
    User getSafetyUser(User originUser);

    /**
     * 根据标签来搜索用户
     *
     * @param tagNameList 标签名列表
     * @return 脱敏后的用户列表
     */
    List<User> searchUsersByTags(List<String> tagNameList);

    /**
     * 获取已登录的用户的信息，未登录则返回 null
     *
     * @param request request对象
     * @return 已登录的用户的信息，未登录则返回 null
     */
    User getLoggedUser(HttpServletRequest request);

    /**
     * 判断登录态用户是否为管理员
     *
     * @param request request对象
     * @return ture: 是管理员   false: 不是管理员
     */
    boolean isAdmin(HttpServletRequest request);

    /**
     * 判断已登录用户是否为管理员
     *
     * @param loggedUser 已登录用户
     * @return ture: 是管理员   false: 不是管理员
     */
    boolean isAdmin(User loggedUser);

}
