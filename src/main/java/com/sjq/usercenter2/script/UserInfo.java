package com.sjq.usercenter2.script;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 从星球表格里导入的用户信息
 */
@Data
public class UserInfo {
    /**
     * 星球编号
     */
    @ExcelProperty
    private String planetCode;

    /**
     * 用户昵称
     */
    @ExcelProperty("成员昵称")
    private String userName;
}
