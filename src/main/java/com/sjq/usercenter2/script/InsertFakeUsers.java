package com.sjq.usercenter2.script;

import com.sjq.usercenter2.mapper.UserMapper;
import com.sjq.usercenter2.model.domain.User;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class InsertFakeUsers {
    @Resource
    public UserMapper userMapper;

    // 定时任务
    // @Scheduled(initialDelay = 5000, fixedRate = Long.MAX_VALUE)
    public void doInsertUsers() {
        final int NUMBER = 10000000;
        for (int i = 0; i < NUMBER; i++) {
            User user = new User();
            // user.setId(0L);
            user.setUsername("fake_user");
            user.setUserAccount("fake_account");
            user.setAvatarUrl("https://avatars.st.dl.eccdnx.com/98bfd7190e2bcf7d624b9a2a41132d438f2c213e_full.jpg");
            user.setGender(0);
            user.setUserPassword("123456789");
            user.setPhone("12345678910");
            user.setEmail("123@163.com");
            user.setUserStatus(0);
            // user.setCreateTime(new Date());
            // user.setUpdateTime(new Date());
            // user.setIsDelete(0);
            user.setUserRole(0);
            user.setPlanetCode("0721");
            user.setTags("[]");
            user.setProfile("我是假用户");
            // 插入
            userMapper.insert(user);
        }
    }
}
