package com.sjq.usercenter2.script;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;

import java.util.List;

/**
 * 从Excel中导入数据
 */
public class ImportExcelData {
    /**
     * 以监听器的方式读取 Excel
     */
    public static void listenerRead(String fileName) {
        EasyExcel.read(fileName, UserInfo.class, new TableListener()).sheet().doRead();
    }

    /**
     * 以同步的方式读取 Excel
     */
    public static List<UserInfo> synchronizedRead(String fileName) {
        List<UserInfo> totalDataList = EasyExcel.read(fileName).head(UserInfo.class).sheet().doReadSync();
        return totalDataList;
    }
}
