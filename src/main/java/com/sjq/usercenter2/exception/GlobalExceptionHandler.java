package com.sjq.usercenter2.exception;

import com.sjq.usercenter2.common.BaseResponse;
import com.sjq.usercenter2.common.ErrorCode;
import com.sjq.usercenter2.common.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理器
 * 在此处理器中定义的异常将被该处理器拦截并处理
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(BusinessException.class)
    public BaseResponse businessExceptionHandler(BusinessException e) {
        log.error("BusinessException: " + e.getMessage(), e);
        return ResultUtil.error(e.getCode(), e.getMessage(), e.getDescription());

    }

    @ExceptionHandler(RuntimeException.class)
    public BaseResponse runtimeExceptionHandler(RuntimeException e) {
        log.error("RuntimeException" + e);
        return ResultUtil.error(ErrorCode.SYSTEM_ERROR, e.getMessage(), "系统内部出现错误");
    }

}
