package com.sjq.usercenter2.service;

import com.sjq.usercenter2.mapper.UserMapper;
import com.sjq.usercenter2.model.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@SpringBootTest
public class InsertFakeUsersTest {
    @Resource
    public UserMapper userMapper;
    @Resource
    public UserService userService;

    @Test
    public void doInsertUsers() {
        final int NUMBER = 10000;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < NUMBER; i++) {
            User user = new User();
            // user.setId(0L);
            user.setUsername("fake_user");
            user.setUserAccount("fake_account");
            user.setAvatarUrl("https://avatars.st.dl.eccdnx.com/98bfd7190e2bcf7d624b9a2a41132d438f2c213e_full.jpg");
            user.setGender(0);
            user.setUserPassword("123456789");
            user.setPhone("12345678910");
            user.setEmail("123@163.com");
            user.setUserStatus(0);
            // user.setCreateTime(new Date());
            // user.setUpdateTime(new Date());
            // user.setIsDelete(0);
            user.setUserRole(0);
            user.setPlanetCode("0721");
            user.setTags("[]");
            user.setProfile("我是假用户");
            userList.add(user);
        }
        userService.saveBatch(userList, 10000);
        stopWatch.stop();
        System.out.println("插入总耗时：" + stopWatch.getTotalTimeMillis());
    }

    /**
     * 并发插入
     */
    @Test
    public void doConcurrentInsertUsers() {
        final int NUMBER = 100000;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        List<CompletableFuture<Void>> futureList = new ArrayList<>(); // 异步任务数组
        for (int i = 0; i < 20; i++) { // 分成10个线程
            List<User> userList = new ArrayList<>();
            int j = 0;
            while (true) { // 往 userList 中添加10000个用户数据
                j++;
                User user = new User();
                user.setUsername("fake_user");
                user.setUserAccount("fake_account");
                user.setAvatarUrl("https://avatars.st.dl.eccdnx.com/98bfd7190e2bcf7d624b9a2a41132d438f2c213e_full.jpg");
                user.setGender(0);
                user.setUserPassword("123456789");
                user.setPhone("12345678910");
                user.setEmail("123@163.com");
                user.setUserStatus(0);
                user.setUserRole(0);
                user.setPlanetCode("0721");
                user.setTags("[]");
                user.setProfile("我是假用户");
                userList.add(user);
                if (j % 5000 == 0) {
                    break;
                }
            }
            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                System.out.println("thread name: " + Thread.currentThread().getName());
                userService.saveBatch(userList, 5000);
            });
            futureList.add(future);
        }
        // 记得调用 join 阻塞当前线程
        CompletableFuture.allOf(futureList.toArray(new CompletableFuture[]{})).join();
        stopWatch.stop();
        System.out.println("插入总耗时：" + stopWatch.getTotalTimeMillis());
    }
}
