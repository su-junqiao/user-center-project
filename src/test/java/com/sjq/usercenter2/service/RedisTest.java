package com.sjq.usercenter2.service;
import java.util.Date;

import com.sjq.usercenter2.model.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import javax.annotation.Resource;

@SpringBootTest
public class RedisTest {

    @Resource
    private RedisTemplate redisTemplate;

    @Test
    void test() {
        ValueOperations valueOperations = redisTemplate.opsForValue();
        // 增
        valueOperations.set("string", "miyagiStrHHHHHH");
        valueOperations.set("Integer", 0721);
        valueOperations.set("Double", 2.1);
        User user = new User();
        user.setId(0L);
        user.setUsername("ximi");
        user.setUserAccount("977967639");
        valueOperations.set("User", user);
        // 查
        Object obj = valueOperations.get("string");
        valueOperations.get("Integer");
        valueOperations.get("Double");
        valueOperations.get("User");
    }
}
